import com.atlassian.jira.component.ComponentAccessor
def attachmentManager = ComponentAccessor.getAttachmentManager()

log.info("Running: Delete small image attachments from desk.com")

attachmentManager.getAttachments(issue).each {
    log.debug( it.getFilename() )

    if (it.getFilesize() <= (5 * 1024) 
        && it.getMimetype().substring(0,6) == 'image/' 
        && it.getFilename().substring(0,4) == 'desk' 
    ) {
        log.info("Deleting small image attachment: " + it.getFilename())
        attachmentManager.deleteAttachment(it)
    }
    
    // If we didn't delete it, let's use trace logging to work out why
    else if( log.level == org.apache.log4j.Level.TRACE ) {
        if (it.getFilesize() <= (5 * 1024) ){
            log.trace("Y: File size is under 5k")
        }
        else {
            log.trace("N: File size is over 5k")
        }

        if (it.getMimetype().substring(0,6) == 'image/'){
            log.trace("Y: File is an image")
        }
        else {
            log.trace("N: File is not an image")
        }

        if (it.getFilename().substring(0,4) == 'desk' ){
            log.trace("Y: File name starts with desk")
        }
        else {
            log.trace("N: File name does not start with desk")
        }

    }
}


