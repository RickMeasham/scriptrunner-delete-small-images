# README #

When integrating Jira with a ticketing system that accepts email, a ticket can become clogged with dozens of small images from email footers. Social media logos are the biggest culprit followed by company logos. The script in this repository is a post-function script you can add to Jira that looks for attachments to delete during the transition. By default the attachments are:

* Images (mimeType begins with 'image/')
* Under 5k
* With a filename that begins with 'desk'

If you're not using desk.com, you might need to change that last one. 

## Log Level ##

* Setting the log level to INFO will record when an image was removed.
* Setting the log level to DEBUG will also show each attachment filename as it processes them
* Setting the log level to TRACE will also show each of the three conditions that are evaluated and whether they matched or not. This is good for testing.

## Warning ##

This script deletes attachments. Just like it says on the wrapper. And once they're gone, they're gone. Use at your own risk.